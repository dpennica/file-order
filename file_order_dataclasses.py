from dataclasses import dataclass
from datetime import datetime

@dataclass()
class Arguments:
    input_dir: str
    output_dir: str
    scan_only: bool
    delete:bool

@dataclass()
class FileProperty:
    hash: str
    created_at: datetime
    updated_at: datetime
    accessed_at: datetime
    path: str
    filename: str
    size: float
    extension: str
    file_stats: str
    file_info: str

@dataclass()
class Statistics:
    total_files: int
    total_files_duplicated: int