import sqlite3
import sys
import os

class CatalogRepository:
    def __init__(self, db_file):
        self.database = db_file

    def connect_database(self):
        conn = None
        try:
            if not os.path.exists(self.database):
                conn = sqlite3.connect(self.database)
                self.create_schema_table(conn)
            else:
                conn = sqlite3.connect(self.database)

        except sqlite3.Error as e:
            print(e)

        return conn

    def create_schema_table(self, conn):
        conn.execute('''CREATE TABLE IF NOT EXISTS FILES 
                 (ID INTEGER PRIMARY KEY AUTOINCREMENT,
                 HASH TEXT NOT NULL UNIQUE,
                 FILENAME TEXT NOT NULL,
                 SIZE INT NOT NULL,
                 CREATED_AT TEXT NOT NULL,
                 UPDATED_AT TEXT NOT NULL,
                 ACCESSED_AT TEXT NOT NULL,
                 SIGNATURE TEXT NOT NULL,
                 PATH TEXT NOT NULL,
                 EXTENSION TEXT NOT NULL,
                 EXIF TEXT
                 );''')
        print("Table FILES created successfully")
        ##########
        conn.execute('''CREATE TABLE IF NOT EXISTS FILES_DUPLICATED 
                     (ID INTEGER PRIMARY KEY AUTOINCREMENT,
                     HASH TEXT NOT NULL,
                     FILENAME TEXT NOT NULL,
                     SIZE INT NOT NULL,
                     CREATED_AT TEXT NOT NULL,
                     UPDATED_AT TEXT NOT NULL,
                     ACCESSED_AT TEXT NOT NULL,
                     SIGNATURE TEXT NOT NULL,
                     PATH TEXT NOT NULL,
                     EXTENSION TEXT NOT NULL,
                     EXIF TEXT
                     );''')
        print("Table FILES_DUPLICATED created successfully")
        #########
        conn.execute('''CREATE TABLE IF NOT EXISTS SIGNATURES 
                     (ID INT PRIMARY KEY     NOT NULL,
                     SIGNATURE TEXT NOT NULL,
                     EXTENSION TEXT NOT NULL
                     );''')
        print("Table FILES created successfully")

    def insert_data(self, conn, file):
        query = '''INSERT INTO FILES ( HASH,FILENAME, SIZE, CREATED_AT, 
           UPDATED_AT, ACCESSED_AT, SIGNATURE, 
           PATH, EXTENSION) VALUES (?,?,?,?,?,?,?,?,?);'''

        file = (file.hash, file.filename, file.size,
                str(file.created_at), str(file.updated_at), str(file.accessed_at),
                "", file.path, file.extension)

        cur = conn.cursor()

        try:
            cur.execute(query, file)
            conn.commit()
            duplicated = False
        except:
            query_dups = '''INSERT INTO FILES_DUPLICATED ( HASH,FILENAME, SIZE, CREATED_AT, 
                   UPDATED_AT, ACCESSED_AT, SIGNATURE, 
                   PATH, EXTENSION) VALUES (?,?,?,?,?,?,?,?,?);'''
            cur.execute(query_dups, file)
            conn.commit()
            duplicated = True

        return cur.lastrowid, duplicated

