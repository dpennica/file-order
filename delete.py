import argparse
from generate_catalog import GenerateCatalog
from utils import confirm

if __name__ == '__main__':
    #### parse arguments
    parser = argparse.ArgumentParser(description='''Scan files from input directory and generate a database 
                                                    Indicate an action (scan, move, delete)''')
    parser.add_argument('-d', '--delete', action='store_true', dest="delete", default=False,
                        help='Delete duplicated files, read from database.')

    args = parser.parse_args()
    gc = GenerateCatalog()

    first_confirm = confirm("Procede with delete of duplicated files?", resp=False)
    if not first_confirm:
        print("Do not proceed")
        exit()

    second_confirm = confirm("Are you sure?", resp=False)
    if not second_confirm:
        print("Do not proceed")
        exit()

    print("Proceed to delete files")
    # print("Statistics")
    # print("total files : {}".format(statistics.get("total_files")))
    # print("duplicates: {}".format(statistics.get("total_files_duplicated")))
    # print("uniques : {}".format(statistics.get("total_files_unique")))

