import argparse
from generate_catalog import GenerateCatalog
from utils import confirm

if __name__ == '__main__':
    #### parse arguments
    parser = argparse.ArgumentParser(description='''Scan files from input directory and generate a database 
                                                    Indicate an action (scan, move, delete)''')
    parser.add_argument('-i', '--input-dir', type=str, required=True, dest="input_dir",
                        help='Input directory to parse')

    arguments = parser.parse_args()

    gc = GenerateCatalog()

    c = confirm("Proceed with scan?", resp=False)
    if not c:
        print("Do not proceed")
        exit()

    print("Start parsing folders: {}".format(arguments.input_dir))
    statistics = gc.generate(arguments)
    print("Statistics")
    print("total files : {}".format(statistics.get("total_files")))
    print("duplicates: {}".format(statistics.get("total_files_duplicated")))
    print("uniques : {}".format(statistics.get("total_files_unique")))

