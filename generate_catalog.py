from datetime import datetime
import time
from os.path import join, getsize
import hashlib
import platform
import fleep

from catalog_repository import CatalogRepository
from file_order_dataclasses import FileProperty, Statistics

import os

class GenerateCatalog():
    BYTES_TO_READ = 128
    BLOCKSIZE = 65536
    total_files = 0
    total_files_unique = 0
    total_files_duplicated = 0

    def __init__(self):
        self.cr = CatalogRepository('files/data.db')
        self.conn = self.cr.connect_database()

    def read_signature(self, filename):
        with open(filename, "rb") as file:
            return fleep.get(file.read(self.BYTES_TO_READ))

    def generate_hash(self, filename):
        hasher = hashlib.md5()
        with open(filename, 'rb') as afile:
            buf = afile.read(self.BLOCKSIZE)
            while len(buf) > 0:
                hasher.update(buf)
                buf = afile.read(self.BLOCKSIZE)
        return hasher.hexdigest()

    def generate(self, arguments):

        self.problems = []
        if not os.path.isdir(arguments.input_dir):
            exit(1)
        dirs_to_not_scan = ["CSV", "node_modules", "vendor", "__pycache__", "dist", ".idea", "DS_Store"]
        dir_counter = 0

        for (root, dirs, files) in os.walk(arguments.input_dir):
            if os.path.basename(os.path.normpath(root)) not in dirs_to_not_scan:
                dir_counter += 1
                print("total dirs scanned: {}".format(dir_counter), end="\r", flush=True)
                for name in files:
                    try:
                        self._get_file_property(name, root)
                        id, duplicated = self.cr.insert_data(self.conn, FileProperty)
                        if duplicated:
                            self.total_files_duplicated +=1
                        else:
                            self.total_files_unique += 1
                    except:
                        self.problems.append(name)

        print("")
        print("problem with file: {}".format(self.problems))
        print("------------")

        self.statistics = {
            "total_files": self.total_files,
            "total_files_duplicated": self.total_files_duplicated,
            "total_files_unique": self.total_files_unique
        }

        return self.statistics

    def _get_file_property(self, name, root):
        filename = join(root, name)
        statinfo = os.stat(filename)
        file_info = self.read_signature(filename)
        hash = self.generate_hash(filename)
        FileProperty.file_info = file_info
        FileProperty.filename = filename
        FileProperty.file_stats = statinfo
        FileProperty.size = getsize(filename)
        FileProperty.created_at = None
        if platform.system() == 'Windows':
            FileProperty.created_at = os.path.getctime(filename)
        else:
            try:
                # potentially Mac
                FileProperty.created_at = statinfo.st_birthtime
            except AttributeError:
                # We're probably on Linux. No easy way to get creation dates here,
                # so we'll settle for when its content was last modified.
                creationTimesinceEpoc = os.path.getctime(filename)
                # convert time sinch epoch to readable format
                FileProperty.created_at = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(creationTimesinceEpoc))
        FileProperty.updated_at = datetime.fromtimestamp(statinfo.st_mtime)
        FileProperty.accessed_at = datetime.fromtimestamp(statinfo.st_atime)
        FileProperty.path, FileProperty.extension = os.path.splitext(filename)
        FileProperty.hash = hash



